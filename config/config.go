package config

import (
	"os"
	"time"

	"github.com/creasty/defaults"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"go.uber.org/zap"
)

type DatabaseConfig struct {
	DBName     string `mapstructure:"db-name"`
	DBUser     string `mapstructure:"db-user"`
	DBPassword string `mapstructure:"db-password"`
	DBHost     string `default:"localhost" mapstructure:"db-host"`
	DBPort     int    `default:"5432" mapstructure:"db-port"`
}

type AlarmConfig struct {
	Mp3Path         string    `mapstructure:"mp3path"`
	BedLuxThreshold int       `default:"10" mapstructure:"bed-lux-threshold"`
	SleepingTimeMin int       `default:"396" mapstructure:"sleeping-time-min"`
	EndTime         time.Time // default値は06:00だが、直接文字列をmapできないため、別途map
}

type Config struct {
	Db    DatabaseConfig `mapstructure:"database"`
	Alarm AlarmConfig    `mapstructure:"alarm"`

	logger *zap.Logger
}

func NewConfig(l *zap.Logger) (*Config, error) {
	c := &Config{
		logger: l,
	}

	err := defaults.Set(c)
	if err != nil {
		err = errors.Wrap(err, "failed to set default value to Config")
		c.logger.Error(err.Error())
		return nil, err
	}
	// time.Time型のEndTimeにdefaultsライブラリで値を設定できないためここで設定
	end, _ := time.Parse("15:04", "06:00")
	c.Alarm.EndTime = end

	err = c.readConfigToml()
	if err != nil {
		return nil, err
	}

	err = c.checkRequiredParams()
	if err != nil {
		return nil, err
	}

	return c, nil
}

func (c *Config) readConfigToml() error {
	t, err := c.getTomlPath()
	if err != nil {
		return err
	}

	v := viper.New()
	v.SetConfigFile(t)
	err = v.ReadInConfig()
	if err != nil {
		err = errors.Wrap(err, "failed to read setting toml file")
		c.logger.Error(err.Error(), zap.String("file", t))
		return err
	}

	err = c.unmarshalConfig(v)
	if err != nil {
		return err
	}

	return nil
}

func (c *Config) getTomlPath() (string, error) {
	t := os.Getenv("TOML_PATH")
	if _, err := os.Stat(t); err != nil {
		err = errors.Wrap(err, "failed to get config toml path")
		c.logger.Error(err.Error(), zap.String("path", t))
		return "", err
	}

	return t, nil
}

func (c *Config) unmarshalConfig(v *viper.Viper) error {
	err := v.Unmarshal(c)
	if err != nil {
		err = errors.Wrap(err, "failed to unmarshal setting toml file")
		c.logger.Error(err.Error())
		return err
	}

	// time.Time型のEndTimeはviperで直接mapできないため、ここで対応
	var endTime string
	err = v.UnmarshalKey("alarm.end-time", &endTime)
	if err != nil {
		err = errors.Wrap(err, "failed to unmarshal end-time in config")
		c.logger.Error(err.Error())
		return err
	}
	if endTime != "" {
		end, _ := time.Parse("15:04", endTime)
		c.Alarm.EndTime = end
	}

	return nil
}

func (c *Config) checkRequiredParams() error {
	if c.Db.DBName == "" {
		err := errors.New("required parameter(database.db-name) not specified")
		c.logger.Error(err.Error())
		return err
	} else if c.Db.DBUser == "" {
		err := errors.New("required parameter(database.db-user) not specified")
		c.logger.Error(err.Error())
		return err
	} else if c.Db.DBPassword == "" {
		err := errors.New("required parameter(database.db-password) not specified")
		c.logger.Error(err.Error())
		return err
	}

	return nil
}
