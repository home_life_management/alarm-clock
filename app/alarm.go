package app

import (
	"gitlab.com/home_life_management/alarm-clock/service"
	"go.uber.org/zap"
)

type AlarmAppOption struct {
	AlarmService *service.AlarmService
	Logger       *zap.Logger
}

type AlarmApp struct {
	alarmService *service.AlarmService
	logger       *zap.Logger
}

func NewAlarmApp(o AlarmAppOption) *AlarmApp {
	a := &AlarmApp{
		alarmService: o.AlarmService,
		logger:       o.Logger,
	}

	return a
}

func (a *AlarmApp) Start() error {
	b, err := a.alarmService.ShouldAlarm()
	if err != nil {
		return err
	}
	if !b {
		return nil
	}

	err = a.alarmService.Alarm()
	if err != nil {
		return err
	}

	return nil
}
