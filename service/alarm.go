package service

import (
	"os"
	"os/exec"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/home_life_management/common_lib/service/database"
	"go.uber.org/zap"
)

type AlarmServiceOption struct {
	DB *database.PostgresService

	Mp3path         string
	BedLuxThreshold int
	SleepingTimeMin int
	EndTime         time.Time

	Logger *zap.Logger
}

type AlarmService struct {
	db *database.PostgresService

	mp3path         string
	bedLuxThreshold int
	sleepingTimeMin int
	endTime         time.Time

	logger *zap.Logger
}

func NewAlarmService(o AlarmServiceOption) *AlarmService {
	a := &AlarmService{
		db:              o.DB,
		mp3path:         o.Mp3path,
		bedLuxThreshold: o.BedLuxThreshold,
		sleepingTimeMin: o.SleepingTimeMin,
		endTime:         o.EndTime,

		logger: o.Logger,
	}

	return a
}

func (s *AlarmService) ShouldAlarm() (bool, error) {
	err := s.db.Connect()
	if err != nil {
		err = errors.Wrap(err, "failed to connect to DB")
		s.logger.Error(err.Error())
		return false, err
	}

	at, err := s.isAlertTime()
	if err != nil || !at {
		return false, err
	}

	bp, err := s.lteButtonPushedToday()
	if err != nil || bp {
		return false, err
	}

	s.db.Close()

	return true, nil
}

func (s *AlarmService) isAlertTime() (bool, error) {
	// 朝の指定時刻(end-time)より後であればfalse
	now, _ := time.Parse("15:04", time.Now().Format("15:04"))
	if !now.Before(s.endTime) {
		s.logger.Info("not morning", zap.String("morning_end", s.endTime.Format("15:04")))
		return false, nil
	}

	// 昨日の就寝時刻から指定時間経過していなければfalse
	s.logger.Debug("start getting last night bed time")
	bedTime, err := s.db.GetBedTime(time.Now().AddDate(0, 0, -1), s.bedLuxThreshold)
	if err != nil {
		err = errors.Wrap(err, "failed to get last night bed time")
		s.logger.Error(err.Error())
		return false, err
	}
	s.logger.Info("got last night bed time", zap.String("bed_time", bedTime.Format("15:04")))
	if time.Now().Before(bedTime.Add(time.Minute * time.Duration(s.sleepingTimeMin))) {
		s.logger.Info("not time to alarm")
		return false, nil
	}

	s.logger.Info("alarm time")

	return true, nil
}

func (s *AlarmService) lteButtonPushedToday() (bool, error) {
	lteLog, err := s.db.GetLatestLteButtonLog()
	if err != nil {
		err = errors.Wrap(err, "failed to get latest lte button log")
		s.logger.Error(err.Error())
		return true, err
	}
	if sameDate(*lteLog.Created, time.Now()) {
		s.logger.Info("lte button pushed already today")
		return true, nil
	}

	s.logger.Info("lte button not pushed already today")

	return false, nil
}

func (s *AlarmService) Alarm() error {
	s.logger.Info("start alarm")

	// TODO: 実害はなさそうだが下記のmpg321が下記の警告を出すので対応
	// tcgetattr(): Inappropriate ioctl for device
	// tty関連の問題で、コンソールから直接mpg321を実行すると上記警告は生じない
	// ffplayへの置き換えも検討
	cmd := exec.Command("/usr/bin/mpg321", "-q", s.mp3path)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Run()

	s.logger.Info("end alarm")

	return nil
}

func sameDate(date1, date2 time.Time) bool {
	y1, m1, d1 := date1.Date()
	y2, m2, d2 := date2.Date()
	return y1 == y2 && m1 == m2 && d1 == d2
}
