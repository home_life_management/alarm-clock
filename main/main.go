package main

import (
	"github.com/pkg/errors"
	"gitlab.com/home_life_management/alarm-clock/app"
	"gitlab.com/home_life_management/alarm-clock/config"
	"gitlab.com/home_life_management/alarm-clock/service"
	"gitlab.com/home_life_management/common_lib"
	"gitlab.com/home_life_management/common_lib/service/database"
	"go.uber.org/zap"
)

func main() {
	l := common_lib.NewLogger()
	c, err := config.NewConfig(l)
	if err != nil {
		return
	}

	l.Info("started alarm app")

	a, err := initAlarmApp(c, l)
	if err != nil {
		return
	}

	err = a.Start()
	if err != nil {
		return
	}

	l.Info("end alarm app")
}

func initAlarmApp(c *config.Config, l *zap.Logger) (*app.AlarmApp, error) {
	o := &database.DatabaseOption{
		Name:     c.Db.DBName,
		User:     c.Db.DBUser,
		Password: c.Db.DBPassword,
	}
	p, err := database.NewPostgresService(o)
	if err != nil {
		err = errors.Wrap(err, "failed initialize database service")
		l.Error(err.Error())
		return nil, err
	}

	so := service.AlarmServiceOption{
		DB:              p,
		Mp3path:         c.Alarm.Mp3Path,
		BedLuxThreshold: c.Alarm.BedLuxThreshold,
		SleepingTimeMin: c.Alarm.SleepingTimeMin,
		EndTime:         c.Alarm.EndTime,
		Logger:          l,
	}
	s := service.NewAlarmService(so)

	ao := app.AlarmAppOption{
		AlarmService: s,
		Logger:       l,
	}
	a := app.NewAlarmApp(ao)

	return a, nil
}
